; ============================================================================================================================
; File		: wat.au3 (2019.24.07)
; Purpose	: Hides WhatsApp for Windows from taskbar when minimized
; Author	: apozlevich@gmail.com
; ============================================================================================================================

#pragma compile(Out, wat.exe)
#pragma compile(Icon, icon.ico)
#pragma compile(UPX, True)

#include <AutoItConstants.au3>
#include <TrayConstants.au3>

Const $LANG_SHOW    = 'Show WhatsApp'
Const $LANG_SETTING = 'Hide from Taskbar'
Const $LANG_EXIT    = 'Exit this script'
Const $LANG_CLOSE   = 'Close WhatsApp'

Const  $TICK     = 50
Const  $WHATSAPP = 'WhatsApp'
Global $HIDE_WA  = True

AutoItSetOption('TrayMenuMode', 1 + 2); No default menu + no checkboxes
AutoItSetOption('TrayOnEventMode', 1) ; Change to event behavior
TraySetClick(16) ; Show menu only when pressing secondary mouse button

_TrayCreate()

While True
   If WinExists($WHATSAPP) Then
	  AutoItSetOption('TrayIconHide', 0)
	  If BitAND(WinGetState($WHATSAPP), $WIN_STATE_MINIMIZED) And $HIDE_WA Then
		 WinSetState($WHATSAPP, '', @SW_HIDE)
	  EndIf
   Else
	  AutoItSetOption('TrayIconHide', 1)
   EndIf
   Sleep($TICK) ; Wait some time to reduce CPU time
WEnd

Func _TrayCreate()
   Local  $btn_show    = TrayCreateItem($LANG_SHOW)
   Global $btn_setting = TrayCreateItem($LANG_SETTING)
   Local  $btn_exit    = TrayCreateItem($LANG_EXIT)
   Local  $btn_closewa = TrayCreateItem($LANG_CLOSE)

   TrayItemSetState($btn_setting, $HIDE_WA ? $TRAY_CHECKED : $TRAY_UNCHECKED)

   TrayItemSetOnEvent($btn_show,    '_WhatsAppShow')
   TrayItemSetOnEvent($btn_setting, '_SettingToggle')
   TrayItemSetOnEvent($btn_exit,    '_ScriptExit')
   TrayItemSetOnEvent($btn_closewa, '_WhatsAppClose')

   If Not @Compiled Then TraySetIcon('icon.ico')
   TraySetOnEvent($TRAY_EVENT_PRIMARYUP, '_WhatsAppShow')
EndFunc ;==>_TrayCreate

Func _SettingToggle()
   If $HIDE_WA Then
	  WinSetState($WHATSAPP, '', @SW_SHOW)
	  TrayItemSetState($btn_setting, $TRAY_UNCHECKED)
   Else
	  TrayItemSetState($btn_setting, $TRAY_CHECKED)
   EndIf
   $HIDE_WA = Not $HIDE_WA
EndFunc ;==>_SettingToggle

Func _WhatsAppShow()
   WinSetState($WHATSAPP, '', @SW_SHOW)
   WinSetState($WHATSAPP, '', @SW_RESTORE)
   WinActivate($WHATSAPP)
EndFunc ;==>_WhatsAppShow

Func _WhatsAppClose()
   WinClose($WHATSAPP)
EndFunc ;==>_WhatsAppClose

Func _ScriptExit()
   WinSetState($WHATSAPP, '', @SW_SHOW)
   Exit
EndFunc ;==>_ScriptExit